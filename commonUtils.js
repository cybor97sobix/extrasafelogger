module.exports = {
  replyErr: function(res, errCode, message) {
    console.error(message);
    res.writeHead(errCode, message);
    res.end();
  }
};
