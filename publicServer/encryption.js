const crypto = require("crypto");
const elliptic = require("elliptic");
const EdDSA = elliptic.eddsa;
const ec = new EdDSA("ed25519");

class Encryption {
  static encryptData(secret, data) {
    let cipher = crypto.createCipher("aes-256-ctr", secret);
    return cipher.update(data, "utf-8", "hex");
  }

  static decryptData(secret, data) {
    let cipher = crypto.createCipher("aes-256-ctr", secret);
    return cipher.update(data, "hex", "utf-8");
  }

  static generateRandomKeyPair() {
    return this.generateKeyPairFromSecret(
      crypto.randomBytes(32).toString("hex")
    );
  }

  static generateKeyPairFromSecret(secret) {
    return ec.keyFromSecret(secret);
  }
}

module.exports = Encryption;
