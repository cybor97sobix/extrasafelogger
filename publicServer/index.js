const http = require("http");
const Encryption = require("./encryption");
const commonUtils = require("../commonUtils");

http
  .createServer((req, res) => {
    if (req.method == "POST") {
      const metaArray = /\/[A-Z_]{0,20}/g.exec(req.url);
      const meta = metaArray ? metaArray[0] : null;
      if (meta) {
        let body = [];
        req.on("data", chunk => {
          body.push(chunk);
        });
        req.on("end", () => {
          const publicKey = Encryption.generateRandomKeyPair().getPublic();
          const request = http.request(
            {
              host: "localhost",
              port: 8081,
              method: "POST"
            },
            response => {
              if (response.statusCode == 200) {
                res.writeHead(200);
              } else {
                res.writeHead(500, response.statusMessage);
              }
              res.end(Buffer.from(publicKey).toString("hex"));
            }
          );
          request.on("error", err =>
            commonUtils.replyErr(res, 500, err.message)
          );
          request.write(
            Encryption.encryptData(
              Buffer.from(publicKey).toString("hex"),
              Buffer.concat(body).toString("utf-8")
            )
          );
          request.end();
        });
      } else
        commonUtils.replyErr(res, 400, "Meta should be provided as /META_NAME");
    } else commonUtils.replyErr(res, 400, "Only POST is acceptable");
  })
  .listen(8080);

console.log("ExtraSafeLogger\n\nListening at port 8080");
