const http = require("http");
const fs = require("fs");
const sqlite3 = require("sqlite3");
const commonUtils = require("../commonUtils");
const path = require("path");

let db = new sqlite3.Database(path.join(__dirname, "data/db/data.db"), err => {
  if (!err) {
    const server = createHttpServer();
    const dbInitScript = fs
      .readFileSync(path.join(__dirname, "data/dbInitScript.sql"))
      .toString();
    db.exec(dbInitScript, err => {
      if (!err) {
        console.log("DB init complete!");
        server.listen(8081, "localhost");
        console.log(
          "ExtraSafeLogger.LoggerServer\n\nListening localhost at port 8081"
        );
      } else {
        console.err("DB failed to init");
      }
    });
  } else console.error("Db cannot be created!");
});

function createHttpServer() {
  return http.createServer((req, res) => {
    if (req.method == "POST") {
      const metaArray = /\/[A-Z_]{0,20}/g.exec(req.url);
      const meta = metaArray ? metaArray[0] : null;
      if (meta) {
        let body = [];
        req.on("data", chunk => body.push(chunk));
        req.on("end", () => {
          db.run(
            "INSERT INTO Log(logTime, sourceServer, meta, logData) VALUES(?,?,?,?)",
            [
              Date.now(),
              req.socket.remoteAddress,
              meta,
              Buffer.concat(body).toString("utf-8")
            ],
            err => {
              if (!err) {
                res.writeHead(200);
                res.end();
              } else {
                console.error("Error occured while adding data to db", err);
                commonUtils.replyErr(
                  res,
                  500,
                  "Failed writing log to database"
                );
              }
            }
          );
        });
      } else
        commonUtils.replyErr(res, 400, "Meta should be provided as /META_NAME");
    } else commonUtils.replyErr(res, 400, "Only POST is acceptable");
  });
}
