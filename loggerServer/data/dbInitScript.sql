CREATE TABLE IF NOT EXISTS Log(id INTEGER PRIMARY KEY AUTOINCREMENT, logTime INTEGER, sourceServer TEXT, meta TEXT, logData TEXT);
PRAGMA JOURNAL_MODE=WAL;
PRAGMA ENCODING="UTF-8";