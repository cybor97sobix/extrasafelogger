module.exports = {
  generateRecordData: generateRecordData,
  logResponse: logResponse
};

const METAS = [
  "CREATE",
  "READ",
  "UPDATE",
  "DELETE",
  "TEST1",
  "2",
  "3fdsa",
  "FD4",
  "",
  "///"
];

function generateRecordData(requestParams, context, ee, next) {
  requestParams.url = "/" + METAS[Math.floor(Math.random() * 10)];
  requestParams.body = (Math.random() * 0xfffffffff).toString(16).repeat(3);
  return next();
}

function logResponse(requestParams, response, context, ee, next) {
  console.log("", response.body);
  return next(context);
}
